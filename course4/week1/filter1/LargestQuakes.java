
/**
 * Find N-closest quakes
 * 
 * @author Duke Software/Learn to Program
 * @version 1.0, November 2015
 */

import java.util.*;

public class LargestQuakes {
    public ArrayList<QuakeEntry> getLargest(ArrayList<QuakeEntry> quakeData, int howMany) {
        ArrayList<QuakeEntry> ret = new ArrayList<QuakeEntry>();
        Collections.sort(quakeData, new Comparator<QuakeEntry>(){
            @Override
            public int compare(QuakeEntry qe, QuakeEntry qe2){
                double d1 = qe.getMagnitude(); 
                double d2 = qe2.getMagnitude();
                if(d1>d2){
                    return -1;
                }else if(d1==d2){
                    return 0;
                }else{
                    return 1;
                }
            }
        });
        for(int i=0;i<Math.min(howMany, quakeData.size());i++){
            ret.add((QuakeEntry)quakeData.get(i));
        }
        return ret;
    }

    public void findLargestQuakes() {
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size());

        ArrayList<QuakeEntry> large = getLargest(list,5);
        for(QuakeEntry qe : large){
            System.out.println(qe);
        }
        System.out.println("number found: "+large.size());
    }
    
}
