
/**
 * Find N-closest quakes
 * 
 * @author Duke Software/Learn to Program
 * @version 1.0, November 2015
 */

import java.util.*;

public class ClosestQuakes {
    public ArrayList<QuakeEntry> getClosest(ArrayList<QuakeEntry> quakeData, Location current, int howMany) {
        ArrayList<QuakeEntry> ret = new ArrayList<QuakeEntry>();
        Collections.sort(quakeData, new Comparator<QuakeEntry>(){
            @Override
            public int compare(QuakeEntry qe, QuakeEntry qe2){
                double d1 = current.distanceTo(qe.getLocation()); 
                double d2 = current.distanceTo(qe2.getLocation());
                if(d1>d2){
                    return 1;
                }else if(d1==d2){
                    return 0;
                }else{
                    return -1;
                }
            }
        });
        for(int i=0;i<Math.min(howMany, quakeData.size());i++){
            ret.add((QuakeEntry)quakeData.get(i));
        }
        return ret;
    }

    public void findClosestQuakes() {
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedatasmall.atom";
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size());

        Location jakarta  = new Location(-6.211,106.845);

        ArrayList<QuakeEntry> close = getClosest(list,jakarta,3);
        for(int k=0; k < close.size(); k++){
            QuakeEntry entry = close.get(k);
            double distanceInMeters = jakarta.distanceTo(entry.getLocation());
            System.out.printf("%4.2f\t %s\n", distanceInMeters/1000,entry);
        }
        System.out.println("number found: "+close.size());
    }
    
}
