import java.util.*;
import edu.duke.*;

public class EarthQuakeClient {
    public EarthQuakeClient() {
        // TODO Auto-generated constructor stub
    }

    public ArrayList<QuakeEntry> filterByMagnitude(ArrayList<QuakeEntry> quakeData, double magMin) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        for(QuakeEntry qe: quakeData){
            if(qe.getMagnitude() > magMin){
                answer.add(qe);
            }
        }
        return answer;
    }

    public ArrayList<QuakeEntry> filterByDepth(ArrayList<QuakeEntry> quakeData, double minDepth, double maxDepth) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        double depth;
        for(QuakeEntry qe: quakeData){
            depth = qe.getDepth();
            if(depth > minDepth && depth < maxDepth){
                answer.add(qe);
            }
        }
        return answer;
    }

    public ArrayList<QuakeEntry> filterByDistanceFrom(ArrayList<QuakeEntry> quakeData, double distMax, Location from) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        for(QuakeEntry qe : quakeData){
            float distance = from.distanceTo(qe.getLocation());
            if(distance < distMax){
                //System.out.println(distance);
                answer.add(qe);
            }
        }
        return answer;
    }
    public ArrayList<QuakeEntry> filterByPhrase(ArrayList<QuakeEntry> quakeData, String where, String phrase) {
        ArrayList<QuakeEntry> answer = new ArrayList<QuakeEntry>();
        String title;
        for(QuakeEntry qe: quakeData){
            //title = qe.getInfo().toLowerCase();
            title = qe.getInfo();
            switch(where){
                case "start":
                    if(title.startsWith(phrase)){
                      answer.add(qe);
                    }
                    break;
                case "end":
                    if(title.endsWith(phrase)){
                      answer.add(qe);
                    }
                    break;
                case "any":
                    if(title.contains(phrase)){
                      answer.add(qe);
                    }
                    break;
            }
        }
        return answer;
    }

    public void dumpCSV(ArrayList<QuakeEntry> list){
        System.out.println("Latitude,Longitude,Magnitude,Info");
        for(QuakeEntry qe : list){
            System.out.printf("%4.2f,%4.2f,%4.2f,%s\n",
                qe.getLocation().getLatitude(),
                qe.getLocation().getLongitude(),
                qe.getMagnitude(),
                qe.getInfo());
        }
    }

    public void bigQuakes() {
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");
        ArrayList<QuakeEntry> big = filterByMagnitude(list, 5.00);
        for(QuakeEntry qe: big){
            System.out.println(qe);
        }
        System.out.println("Found "+big.size()+" quakes that match that criteria");
    }

    public void quakesOfDepth() {
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");
        ArrayList<QuakeEntry> result = filterByDepth(list, -10000.0, -5000.0);
        for(QuakeEntry qe: result){
            System.out.println(qe);
        }
        System.out.println("Found "+result.size()+" quakes that match that criteria");
    }

    public void quakesByPhrase() {
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");
        //ArrayList<QuakeEntry> result = filterByPhrase(list, "end", "California");
        ArrayList<QuakeEntry> result = filterByPhrase(list, "start", "Explosion");
        //ArrayList<QuakeEntry> result = filterByPhrase(list, "any", "Can");
        for(QuakeEntry qe: result){
            System.out.println(qe);
        }
        System.out.println("Found "+result.size()+" quakes that match that criteria");
    }

    public void closeToMe(){
        EarthQuakeParser parser = new EarthQuakeParser();
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        String source = "data/nov20quakedatasmall.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        System.out.println("read data for "+list.size()+" quakes");

        // This location is Durham, NC
        //Location city = new Location(35.988, -78.907);
        
        // This location is Bridgeport, CA
        Location city =  new Location(38.17, -118.82);
        ArrayList<QuakeEntry> closest = filterByDistanceFrom(list, 1000*1000, city);
        for(QuakeEntry qe : closest){
            System.out.println(city.distanceTo(qe.getLocation())/1000 +" " +qe.getInfo());
        }
        System.out.println("Found "+closest.size()+" quakes that match that criteria");
    }

    public void createCSV(){
        EarthQuakeParser parser = new EarthQuakeParser();
        String source = "data/nov20quakedatasmall.atom";
        //String source = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
        ArrayList<QuakeEntry> list  = parser.read(source);
        dumpCSV(list);
        System.out.println("# quakes read: " + list.size());
        for (QuakeEntry qe : list) {
            System.out.println(qe);
        }
    }
    public static void main(String[] args){
        //EarthQuakeClient client = new EarthQuakeClient();
        //client.createCSV();
        //client.bigQuakes();
        //client.closeToMe();
        //client.quakesOfDepth();
        //client.quakesByPhrase();
        //ClosestQuakes close = new ClosestQuakes();
        //close.findClosestQuakes();
        LargestQuakes large = new LargestQuakes();
        large.findLargestQuakes();
    }
    
}
