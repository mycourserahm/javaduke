import edu.duke.*;

class WordLengths{
    public void countWordLengths(FileResource resource, int[] counts){
        for(String word: resource.words()){
            int wordLength = word.length();
            if (!Character.isLetter(word.charAt(0)))
                wordLength -= 1;
            if (!Character.isLetter(word.charAt(word.length()-1)))
                wordLength -= 1;
            if(wordLength < 0) wordLength = 0;

            if(wordLength>=counts.length){
                counts[counts.length] += 1;
            }else{
                counts[wordLength] += 1;
            }
        }
    }

    public void testCountWordLengths(){
        FileResource fr = new FileResource();
        int[] counts = new int[31];
        countWordLengths(fr, counts);
        for(int k=0;k<counts.length;k++){
            System.out.println(k + "\t" + counts[k]);
        }
    }

    public static void main(String[] args){
        WordLengths wl = new WordLengths();
        wl.testCountWordLengths();
    }
}

