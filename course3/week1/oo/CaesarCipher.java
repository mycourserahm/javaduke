import edu.duke.*;

class CaesarCipher{
    private String alphabet;
    private String shiftedAlphabet;
    private int mainKey;

    public CaesarCipher(int key){
        alphabet = "abcdefghijklmnopqrstuvwxyz";
        mainKey = key;
        shiftAlphabet(key);
    }

    private void shiftAlphabet(int key){
        shiftedAlphabet = alphabet.substring(key) + alphabet.substring(0, key);
    }

    public String encrypt(String input){
        StringBuilder sb = new StringBuilder(input);
        for (int i=0;i<sb.length();i++) {
            char ch = sb.charAt(i);
            boolean isUpper = Character.isUpperCase(ch);
            ch = isUpper?Character.toLowerCase(ch):ch;
            if(alphabet.indexOf(ch) != -1){
                char newch = shiftedAlphabet.charAt(alphabet.indexOf(ch));
                newch = isUpper?Character.toUpperCase(newch):newch;
                sb.setCharAt(i, newch);
            }
        }
        return sb.toString();
    }

    public String decrypt(String input){
        String tmpShifted = shiftedAlphabet;
        int tmpKey = 26 - mainKey;
        shiftAlphabet(tmpKey);
        String decrypted = encrypt(input);
        shiftedAlphabet = tmpShifted;
        return decrypted;
    }

    public String encryptTwoKeys(String input, int key1, int key2){
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String code1 = alphabet.substring(key1) + alphabet.substring(0, key1);
        String code2 = alphabet.substring(key2) + alphabet.substring(0, key2);
        StringBuilder sb = new StringBuilder(input);
        for (int i=0;i<sb.length();i++) {
            char ch = sb.charAt(i);
            boolean isUpper = Character.isUpperCase(ch);
            ch = isUpper?Character.toLowerCase(ch):ch;
            if(alphabet.indexOf(ch) != -1){
                String code = ((i&1)==0)?code1:code2;
                char newch = code.charAt(alphabet.indexOf(ch));
                newch = isUpper?Character.toUpperCase(newch):newch;
                sb.setCharAt(i, newch);
            }
        }
        return sb.toString();

    }

    public static void main(String[] args){
        testCaesarCipher test = new testCaesarCipher();
        //cc.testEncrypt();
        test.simpleTests();
    }
}

class testCaesarCipher{
    
    public int[] countLetters(String message){
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k<message.length(); k++){
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alpha.indexOf(ch);
            if(dex != -1){
                counts[dex] += 1;
            }
        }
        return counts;
    }

    public int maxIndex(int[] vals){
        int maxDex = 0;
        for(int k=0; k< vals.length; k++){
            if(vals[k] > vals[maxDex]){
                maxDex = k;
            }
        }
        return maxDex;
    }

    public void simpleTests(){
        FileResource fr = new FileResource();
        String message = fr.asString();
        CaesarCipher cc = new CaesarCipher(18);
        String encrypted = cc.encrypt(message);
        System.out.println(encrypted);
        //String decrypted = cc.decrypt(encrypted);
        String decrypted = breakCaesarCipher(encrypted);
        System.out.println(decrypted);
    }

    
    public String breakCaesarCipher(String input){
        int[] freqs = countLetters(input);
        int maxDex = maxIndex(freqs);
        int dkey = maxDex - 4;
        if(maxDex < 4){
            dkey = 26 - (4 - maxDex);
        }
        CaesarCipher cc = new CaesarCipher(26-dkey);
        return cc.encrypt(input);
    }
}

