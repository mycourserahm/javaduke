import edu.duke.*;

class CaesarCipherTwo{
    private String alphabet;
    private String shiftedAlphabet1;
    private String shiftedAlphabet2;
		private int key1;
		private int key2;

    public CaesarCipherTwo(int k1, int k2){
        alphabet = "abcdefghijklmnopqrstuvwxyz";
        shiftedAlphabet1 = shiftAlphabet(k1);
        shiftedAlphabet2 = shiftAlphabet(k2);
				key1 = k1;
				key2 = k2;
    }
    private String shiftAlphabet(int key){
        return alphabet.substring(key) + alphabet.substring(0, key);
    }

    public String encrypt(String input){
				boolean isUpper;
				char ch, newch;
        StringBuilder sb = new StringBuilder(input);
        for (int i=0;i<sb.length();i++) {
            ch = sb.charAt(i);
            isUpper = Character.isUpperCase(ch);
            ch = isUpper?Character.toLowerCase(ch):ch;
            if(alphabet.indexOf(ch) != -1){
                String code = ((i&1)==0)?shiftedAlphabet1:shiftedAlphabet2;
                newch = code.charAt(alphabet.indexOf(ch));
                newch = isUpper?Character.toUpperCase(newch):newch;
                sb.setCharAt(i, newch);
            }
        }
        return sb.toString();
    }

    public String decrypt(String input){
        CaesarCipherTwo cct = new CaesarCipherTwo(26-key1, 26-key2);
        return cct.encrypt(input);
    }

    public static void main(String[] args){
			testCaesarCipherTwo test = new testCaesarCipherTwo();
			test.simpleTests();
			//test.testString();
			//test.testStringBreak();
    }
}

class testCaesarCipherTwo{
    public int[] countLetters(String message){
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k<message.length(); k++){
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alpha.indexOf(ch);
            if(dex != -1){
                counts[dex] += 1;
            }
        }
        return counts;
    }

    public int maxIndex(int[] vals){
        int maxDex = 0;
        for(int k=0; k< vals.length; k++){
            if(vals[k] > vals[maxDex]){
                maxDex = k;
            }
        }
        return maxDex;
    }
    public String halfOfString(String message, int start){
        StringBuilder sb = new StringBuilder();
        for(int i=start; i<message.length();i+=2){
            sb.append(message.charAt(i));
        }
        return sb.toString();
    }

    public int getKey(String s){
        int[] freqs = countLetters(s);
        int key = maxIndex(freqs);
        int dkey = key -4;
        if(key < 4){
            dkey = 26 - (4 - key);
        }
        return dkey;

    }
		public void simpleTests(){
				FileResource fr = new FileResource();
				String message = fr.asString();
				//CaesarCipherTwo cct = new CaesarCipherTwo(17, 3);
				//String encrypted = cct.encrypt(message);
				//System.out.println(encrypted);
				//String decrypted = cct.decrypt(message);
				System.out.println(breakCaesarCipher(message));
		}
			
		public void testString(){
				CaesarCipherTwo cct = new CaesarCipherTwo(14, 24);
				String decrypted = cct.decrypt("Hfs cpwewloj loks cd Hoto kyg Cyy.");
				System.out.println(decrypted);
		}

		public void testStringBreak(){
				System.out.println(breakCaesarCipher("Aal uttx hm aal Qtct Fhljha pl Wbdl. Pvxvxlx!"));
		}
    public String breakCaesarCipher(String input){
        String aHalf = halfOfString(input, 0);
        String bHalf = halfOfString(input, 1);
        int aKey = getKey(aHalf);
        int bKey = getKey(bHalf);
        System.out.println("encrypt keys are: " + aKey + " " + bKey);
        CaesarCipherTwo cct = new CaesarCipherTwo(aKey, bKey);
        return cct.decrypt(input);
    }

}
