import edu.duke.*;

class CaesarBreaker{
    public int[] countLetters(String message){
        String alpha = "abcdefghijklmnopqrstuvwxyz";
        int[] counts = new int[26];
        for(int k=0; k<message.length(); k++){
            char ch = Character.toLowerCase(message.charAt(k));
            int dex = alpha.indexOf(ch);
            if(dex != -1){
                counts[dex] += 1;
            }
        }
        return counts;
    }

    public int maxIndex(int[] vals){
        int maxDex = 0;
        for(int k=0; k< vals.length; k++){
            if(vals[k] > vals[maxDex]){
                maxDex = k;
            }
        }
        return maxDex;
    }

    public String decrypt(String encrypted){
        CaesarCipher cc = new CaesarCipher();
        int[] freqs = countLetters(encrypted);
        int maxDex = maxIndex(freqs);
        int dkey = maxDex - 4;
        if(maxDex < 4){
            dkey = 26 - (4 - maxDex);
        }
        return cc.encrypt(encrypted, 26-dkey);
    }

    public String halfOfString(String message, int start){
        StringBuilder sb = new StringBuilder();
        for(int i=start; i<message.length();i+=2){
            sb.append(message.charAt(i));
        }
        return sb.toString();
    }

    public int getKey(String s){
        int[] freqs = countLetters(s);
        int key = maxIndex(freqs);
        int dkey = key -4;
        if(key < 4){
            dkey = 26 - (4 - key);
        }
        return dkey;

    }

    public String decryptTwoKeys(String encrypted){
        CaesarCipher cc = new CaesarCipher();
        String aHalf = halfOfString(encrypted, 0);
        String bHalf = halfOfString(encrypted, 1);
        int aKey = getKey(aHalf);
        int bKey = getKey(bHalf);
        System.out.println("encrypt keys are: " + aKey + " " + bKey);
        return cc.encryptTwoKeys(encrypted, 26-aKey, 26-bKey);
    }

    public void testDecrypt(){
        CaesarCipher cc = new CaesarCipher();
        String encrypted = cc.encrypt("Original message", 15);
        System.out.println(cc.encrypt(encrypted, 11));
    }
    public void testHalfString(){
        System.out.println(halfOfString("121212121",0));
        System.out.println(halfOfString("121212121",1));
    }
    public void testDecryptTwo(){
       // System.out.println(decryptTwoKeys("Akag tjw Xibhr awoa aoee xakex znxag xwko"));
       FileResource fr = new FileResource();
       String enc = fr.asString();
       System.out.println(decryptTwoKeys(enc));
    }
    public static void main(String[] args){
        CaesarBreaker cb = new CaesarBreaker();
        //cb.testDecrypt();
        //cb.testHalfString();
        cb.testDecryptTwo();

    }
}
