class WordPlay{
    public boolean isVowel(char ch){
        return "aeiouAEIOU".indexOf(ch) != -1;
    }

    public String replaceVowel(String phrase, char ch){
        String vowel = "aeiouAEIOU";
        for(int i=0;i<vowel.length();i++){
            phrase = phrase.replace(vowel.charAt(i), ch);
        }
        return phrase;
            
    }
    public String emphasize(String phrase, char ch){
        StringBuilder sb = new StringBuilder(phrase);
        for(int i=0;i<phrase.length();i++){
            if(Character.toLowerCase(phrase.charAt(i)) == ch){
                if((i&1) == 0) {sb.setCharAt(i, '*');}
                else {sb.setCharAt(i, '+');}
            }
        }
        return sb.toString();
    }
    public void testIsVowel(){
        System.out.println("F is vowel:" + isVowel('F'));
        System.out.println("a is vowel:" + isVowel('a'));
    }
    public void testReplaceVowel(){
        System.out.println(replaceVowel("Hello World!", '*'));
    }
    public void testEmphasize(){
        System.out.println(emphasize("Mary Bella Abracadabra", 'a'));
    }
    
    public static void main(String[] args){
        WordPlay wp = new WordPlay();
        wp.testIsVowel();
        wp.testReplaceVowel();
        wp.testEmphasize();
    }
}

