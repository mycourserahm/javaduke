class CaesarCipher{
    public String encrypt(String input, int key){
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String code = alphabet.substring(key) + alphabet.substring(0, key);
        StringBuilder sb = new StringBuilder(input);
        for (int i=0;i<sb.length();i++) {
            char ch = sb.charAt(i);
            boolean isUpper = Character.isUpperCase(ch);
            ch = isUpper?Character.toLowerCase(ch):ch;
            if(alphabet.indexOf(ch) != -1){
                char newch = code.charAt(alphabet.indexOf(ch));
                newch = isUpper?Character.toUpperCase(newch):newch;
                sb.setCharAt(i, newch);
            }
        }
        return sb.toString();

    }
    public String encryptTwoKeys(String input, int key1, int key2){
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String code1 = alphabet.substring(key1) + alphabet.substring(0, key1);
        String code2 = alphabet.substring(key2) + alphabet.substring(0, key2);
        StringBuilder sb = new StringBuilder(input);
        for (int i=0;i<sb.length();i++) {
            char ch = sb.charAt(i);
            boolean isUpper = Character.isUpperCase(ch);
            ch = isUpper?Character.toLowerCase(ch):ch;
            if(alphabet.indexOf(ch) != -1){
                String code = ((i&1)==0)?code1:code2;
                char newch = code.charAt(alphabet.indexOf(ch));
                newch = isUpper?Character.toUpperCase(newch):newch;
                sb.setCharAt(i, newch);
            }
        }
        return sb.toString();

    }

    public void testEncrypt(){
        System.out.println(encrypt("Can you imagine life WITHOUT the internet AND computers in your pocket?", 15));
    }
    public void testTwoKeys(){
        System.out.println(encryptTwoKeys("Can you imagine life WITHOUT the internet AND computers in your pocket?", 21, 8));
        //System.out.println(encryptTwoKeys("Top ncmy qkff vi vguv vbg ycpx", 24, 6));
    }
    public static void main(String[] args){
        CaesarCipher cc = new CaesarCipher();
        cc.testEncrypt();
        cc.testTwoKeys();
    }
}
