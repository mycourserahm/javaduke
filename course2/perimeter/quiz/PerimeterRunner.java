import edu.duke.*;

public class PerimeterRunner {
    public double getPerimeter (Shape s) {
        // Start with totalPerim = 0
        double totalPerim = 0.0;
        // Start wth prevPt = the last point 
        Point prevPt = s.getLastPoint();
        // For each point currPt in the shape,
        for (Point currPt : s.getPoints()) {
            // Find distance from prevPt point to currPt 
            double currDist = prevPt.distance(currPt);
            // Update totalPerim by currDist
            totalPerim = totalPerim + currDist;
            // Update prevPt to be currPt
            prevPt = currPt;
        }
        // totalPerim is the answer
        return totalPerim;
    }
    public int getNumPoints(Shape s) {
        int totalNums = 0;
        for (Point currPt: s.getPoints()) {
            totalNums += 1;
        }
        return totalNums;
    }
    public double getAverageLength(Shape s) {
        int counts = 0;
        double totalPerim = 0;
        Point prevPt = s.getLastPoint();
        for (Point currPt: s.getPoints()) {
            counts += 1;
            totalPerim += prevPt.distance(currPt);
            prevPt = currPt;
        }
        return totalPerim/counts;
    }
    
    public double getLargestSide(Shape s){
        double largest = 0;
        Point prevPt = s.getLastPoint();
        for (Point currPt: s.getPoints()) {
            double currLength = prevPt.distance(currPt);
            if (currLength > largest){
                largest = currLength;
            }
            prevPt = currPt;
        }
        return largest;
    }
    
    public double getLargestX(Shape s) {
        double largestX = 0;
        for(Point currPt: s.getPoints()) {
            double currX = currPt.getX();
            if (currX > largestX) {
                largestX = currX;
            }
        }
        return largestX;
    }
    
    public void testPerimeter () {
        FileResource fr = new FileResource("datatest5.txt");
        Shape s = new Shape(fr);
        double length = getPerimeter(s);
        int count = getNumPoints(s);
        double aveLength = getAverageLength(s);
        double largestSide = getLargestSide(s);
        double largestX = getLargestX(s);
        System.out.println("perimeter = " + length);
        System.out.println("pointcount = " + count);
        System.out.println("average length = " + aveLength);
        System.out.println("Longest = " + largestSide);
        System.out.println("Largestx = " + largestX);
    }

    public static void main (String[] args) {
        PerimeterRunner pr = new PerimeterRunner();
        pr.testPerimeter();
    }
}
